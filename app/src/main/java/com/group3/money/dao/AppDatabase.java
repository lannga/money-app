package com.group3.money.dao;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.group3.money.entity.DCategory;
import com.group3.money.entity.VCategory;
import com.group3.money.entity.DGroup;
import com.group3.money.entity.DTransaction;
import com.group3.money.entity.VTransaction;

@Database(entities = {DCategory.class, DTransaction.class, DGroup.class},
        views = {VCategory.class, VTransaction.class},
        version = 1,
        exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "app_db";

    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract DCategoryAccess dCategoryAccess();

    public abstract DTransactionAccess dTransactionAccess();
    public abstract VTransactionAccess vTransactionAccess();

    public abstract DGroupAccess dGroupAccess();

    public abstract VCategoryAccess vCategoryAccess();
}
