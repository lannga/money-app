package com.group3.money.dao;

import androidx.room.Dao;
import androidx.room.Query;
import com.group3.money.entity.VTransaction;
import com.group3.money.modals.Total;

import java.util.List;

@Dao
public interface VTransactionAccess {

//    @Query("SELECT * FROM VTransaction WHERE categoryId = :categoryId")
//    void getTransaction(String categoryId);
//
//    @Query("SELECT categoryId, categoryIcon, categoryName,type, SUM(amount) as total FROM VTransaction WHERE categoryId = :categoryId OR rootCategoryId = :rootCategoryId")
//    void getStatistic(String categoryId, String rootCategoryId);


    @Query("SELECT * FROM VTransaction")
    List<VTransaction> getAll();

    @Query("SELECT * FROM VTransaction " +
            "WHERE groupId =:groupId AND (categoryId =:id OR rootCategoryId = :id ) AND time >= :fromTime AND time <= :toTime ")
    List<VTransaction>  getListByCat(String groupId,String id, long fromTime, long toTime);

    @Query("SELECT * FROM VTransaction " +
            "WHERE groupId =:groupId AND time >= :fromTime AND time <= :toTime ")
    List<VTransaction>  getList(String groupId, long fromTime, long toTime);

    @Query("SELECT * FROM VTransaction " +
            "WHERE groupId =:groupId AND type =:type AND time >= :fromTime AND time <= :toTime ")
    List<VTransaction>  getListByType(String groupId,int type, long fromTime, long toTime);

    @Query("SELECT * FROM VTransaction " +
            "WHERE groupId =:groupId AND (categoryId =:id OR rootCategoryId = :id )AND type =:type AND time >= :fromTime AND time <= :toTime ")
    List<VTransaction>  getListByTypeCat(String groupId,String id, long fromTime, long toTime, int type);

    @Query("SELECT SUM(amount) as value FROM VTransaction " +
            "WHERE groupId =:groupId AND (categoryId =:id OR rootCategoryId = :id ) AND time >= :fromTime AND time < :toTime  ")
    Total sumById(String groupId, String id, long fromTime, long toTime);


}
