package com.group3.money.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.group3.money.entity.DGroup;

import java.util.List;

@Dao
public interface DGroupAccess {

    @Insert
    void insert(DGroup... groups);

    @Update
    void update(DGroup... groups);

    @Delete
    void delete(DGroup... groups);

    @Query("SELECT * FROM `DGroup` WHERE id = :id")
    List<DGroup> getById(String id);

    @Query("SELECT * FROM `DGroup`")
    List<DGroup> getAll();
}
