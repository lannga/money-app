package com.group3.money.dao;

import androidx.room.Dao;
import androidx.room.Query;

import com.group3.money.entity.VCategory;

import java.util.List;

@Dao
public interface VCategoryAccess {

    @Query("SELECT * FROM VCategory WHERE groupId = :groupId")
    List<VCategory> getAll (String groupId);

    @Query("SELECT * FROM VCategory WHERE groupId =:groupId AND rootId IS NULL")
    List<VCategory> getRoot (String groupId);

    @Query("SELECT * FROM VCategory WHERE groupId =:groupId AND rootId IS NULL AND type =:type")
    List<VCategory> getRootByType(String groupId,int type);

    @Query("SELECT * FROM VCategory WHERE groupId =:groupId AND rootId = :rootId")
    List<VCategory> getByRootId (String groupId, String rootId);
}


