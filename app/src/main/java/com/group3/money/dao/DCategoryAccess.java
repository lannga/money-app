package com.group3.money.dao;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.group3.money.entity.DCategory;

import java.util.List;

@Dao
public interface DCategoryAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(DCategory... categories);

    @Update
    void update(DCategory... categories);

    @Delete
    void delete(DCategory... categories);

    @Query("DELETE FROM `DCategory`")
    void deleteAll();

    @Query("SELECT * FROM `DCategory` WHERE groupId =:groupId")
    List<DCategory> getAll(String groupId);

    @Query("SELECT * FROM `DCategory` WHERE groupId =:groupId AND rootId IS NULL")
    List<DCategory> getRoot(String groupId);

    @Query("SELECT * FROM `DCategory` WHERE groupId =:groupId AND rootId =:catId")
    List<DCategory> getByRootId(String groupId, String catId);

    @Query("SELECT * FROM `DCategory` WHERE groupId =:groupId AND (rootId =:rootId || rootId IS NULL) AND type =:type AND name =:name")
    List<DCategory> getCheck(String groupId, String rootId, int type, String name);

}
