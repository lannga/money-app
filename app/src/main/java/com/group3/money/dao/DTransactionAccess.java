package com.group3.money.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.group3.money.entity.DTransaction;

import java.util.List;


@Dao
public interface DTransactionAccess {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert (DTransaction... transactions);

    @Update
    void update (DTransaction... transactions);

    @Delete
    void delete (DTransaction... transactions);

    @Query("DELETE FROM `DTransaction` WHERE groupId = :groupId")
    void deleteByCategory(int groupId);

    @Query("DELETE FROM `DTransaction` WHERE groupId = :groupId AND categoryId = :categoryId")
    void deleteByCategory(int groupId, int categoryId);

    @Query("SELECT * FROM `DTransaction` ")
    List<DTransaction> getAll();

    @Query("SELECT * FROM `DTransaction` WHERE groupId = :groupId AND type =:type")
    List<DTransaction> getBySearch(int groupId, int type);

    @Query("SELECT * FROM `DTransaction` WHERE groupId = :groupId AND type =:type AND categoryId =:categoryId")
    List<DTransaction> getBySearch(int groupId, int type, int categoryId);

    @Query("SELECT * FROM `DTransaction` WHERE groupId = :groupId AND type =:type AND categoryId =:categoryId AND time >= :fromTime AND time <= :toTime ")
    List<DTransaction> getBySearch(int groupId, int type, int categoryId, long fromTime, long toTime);


}
