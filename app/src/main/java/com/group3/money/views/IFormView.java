package com.group3.money.views;

public interface IFormView {

    void showError(String message);

    void navigateBack(Object obj);
}
