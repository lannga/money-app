package com.group3.money.views;

import java.util.Calendar;

public interface IChoseDateView {
    void choseDate(Calendar calendar);
}
