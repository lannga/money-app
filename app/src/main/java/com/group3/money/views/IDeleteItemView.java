package com.group3.money.views;

public interface IDeleteItemView {

    void showNotification(int position);
    void confirmDelete();
    void cancelDelete();
}
