package com.group3.money.presenters;

import android.content.Context;

import com.group3.money.dao.AppDatabase;
import com.group3.money.dao.VCategoryAccess;
import com.group3.money.entity.VCategory;

import com.group3.money.dao.DCategoryAccess;
import com.group3.money.entity.DCategory;
import com.group3.money.views.IFormView;

import java.util.LinkedList;
import java.util.List;

public class CategoryFormPresenter {
    private IFormView iFormView;
    private DCategoryAccess dCategoryAccess;
    private VCategoryAccess vCategoryAccess;

    public CategoryFormPresenter(IFormView iFormView, Context context) {
        this.iFormView = iFormView;
        AppDatabase appDatabase = AppDatabase.getInstance(context);
        dCategoryAccess = appDatabase.dCategoryAccess();
        vCategoryAccess = appDatabase.vCategoryAccess();
    }

    public List<VCategory> getRootByType(String groupId, int i) {
        try {
            return vCategoryAccess.getRootByType(groupId, i);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public void create(final DCategory category) {

        if (category.getName() == null) {
            iFormView.showError("Tên Category chưa được nhập");
            return;
        }

        final List<DCategory> dCategories = new LinkedList<>();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                List<DCategory> checks = dCategoryAccess.getCheck(category.getGroupId(), category.getRootId(), category.getType(), category.getName());
                dCategories.addAll(checks);

            }
        });
        thread.start();
        try {
            thread.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!dCategories.isEmpty()) {
            iFormView.showError("Category đã tồn tại");
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                dCategoryAccess.insert(category);
                iFormView.navigateBack(category);
            }
        }).start();


    }

}
