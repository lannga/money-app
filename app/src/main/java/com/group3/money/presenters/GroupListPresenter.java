package com.group3.money.presenters;

import android.content.Context;

import com.group3.money.dao.AppDatabase;
import com.group3.money.dao.DGroupAccess;
import com.group3.money.views.IListView;

public class GroupListPresenter {

    private DGroupAccess dGroupAccess;
    private IListView iListView;

    public GroupListPresenter(IListView iListView, Context context) {
        this.iListView = iListView;
        dGroupAccess = AppDatabase.getInstance(context).dGroupAccess();
    }

    public void getList() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                iListView.showList(dGroupAccess.getAll());
            }
        }).start();

    }
}
