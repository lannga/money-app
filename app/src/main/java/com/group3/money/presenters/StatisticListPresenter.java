package com.group3.money.presenters;

import android.content.Context;

import com.group3.money.dao.AppDatabase;
import com.group3.money.dao.DCategoryAccess;
import com.group3.money.dao.DTransactionAccess;
import com.group3.money.dao.VTransactionAccess;
import com.group3.money.entity.DCategory;
import com.group3.money.entity.DTransaction;
import com.group3.money.entity.VStatistic;
import com.group3.money.modals.Total;
import com.group3.money.utils.Constants;
import com.group3.money.utils.Format;
import com.group3.money.views.IListView;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class StatisticListPresenter {

    private IListView iListView;
    private VTransactionAccess vTransactionAccess;
    private DTransactionAccess dTransactionAccess;
    private DCategoryAccess dCategoryAccess;


    public StatisticListPresenter(IListView iListView, Context context) {
        this.iListView = iListView;
        AppDatabase appDatabase = AppDatabase.getInstance(context);
        vTransactionAccess = appDatabase.vTransactionAccess();
        dTransactionAccess = appDatabase.dTransactionAccess();
        dCategoryAccess = appDatabase.dCategoryAccess();
    }

    public void getList(final String groupId, Calendar calendar, int index){
        long[] time = Format.getFromAndTo(calendar, index);
        final long fromTime = time[0];
        final long toTime = time[1];
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<DTransaction> dTransactions = dTransactionAccess.getAll();
                List<DCategory> categories = arrangeCategories(groupId);
                List<VStatistic> expenses = new LinkedList<>();
                List<VStatistic> incomes = new LinkedList<>();
                for(DCategory d : categories){
                    VStatistic vStatistic = new VStatistic(d);
                    Total total = vTransactionAccess.sumById(groupId,d.getId(), fromTime, toTime);
                    if(total.value == 0) continue;
                    vStatistic.setTotal(total.value);
                    if(d.getType() == Constants.EXPENSE){
                        expenses.add(vStatistic);
                    }else if(d.getType() == Constants.INCOME){
                        incomes.add(vStatistic);
                    }
                }
                iListView.showList(expenses, incomes);
            }
        }).start();
    }

    public List<DCategory> arrangeCategories(String groupId){
        List<DCategory> finalList = new LinkedList<>();
        List<DCategory> rootCats = dCategoryAccess.getRoot(groupId);
        for(DCategory d : rootCats){
            finalList.add(d);
            List<DCategory> children = dCategoryAccess.getByRootId(groupId, d.getId());
            if(children.size() <= 0) continue;
            finalList.addAll(children);
        }
        return finalList;
    }
}
