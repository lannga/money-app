package com.group3.money.presenters;

import android.content.Context;

import com.group3.money.dao.AppDatabase;
import com.group3.money.dao.DTransactionAccess;
import com.group3.money.dao.VCategoryAccess;
import com.group3.money.entity.DTransaction;
import com.group3.money.entity.VCategory;
import com.group3.money.views.IFormView;

import java.util.LinkedList;
import java.util.List;

public class TransactionFormPresenter{

    private IFormView iFormView;
    private DTransactionAccess dTransactionAccess;
    private VCategoryAccess vCategoryAccess;

    public TransactionFormPresenter(IFormView iFormView, Context context) {
        this.iFormView = iFormView;
        AppDatabase appDatabase = AppDatabase.getInstance(context);
        dTransactionAccess = appDatabase.dTransactionAccess();
        vCategoryAccess = appDatabase.vCategoryAccess();
    }

    public void insert(final DTransaction transaction) {
        String message = "";
        if(transaction.getType() == 0){
            message = "chọn loại giao dịch";
        }else if (transaction.getCategoryId() == null){
            message = "chọn category";
        }else if (transaction.getAmount() <= 0) {
            message = "nhập số tiền";
        }else{
            new Thread(new Runnable() {
                @Override
                public void run() {
                    dTransactionAccess.insert(transaction);
                    iFormView.navigateBack(null);
                }
            }).start();
            return;
        }
        iFormView.showError(message);
    }

    public List<VCategory> getCategories(String groupId, int i){
        try {
            List<VCategory> categoryRoots = vCategoryAccess.getRootByType(groupId,i);
            List<VCategory> categoryList = new LinkedList<>();
            for (VCategory v: categoryRoots) {
                categoryList.add(v);
                List<VCategory> categoryChildren = vCategoryAccess.getByRootId(groupId,v.getId());
                if(categoryChildren.isEmpty()) continue;
                categoryList.addAll(categoryChildren);
            }
            return categoryList;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
