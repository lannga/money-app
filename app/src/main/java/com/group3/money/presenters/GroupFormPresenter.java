package com.group3.money.presenters;

import android.content.Context;

import com.group3.money.dao.AppDatabase;
import com.group3.money.dao.DGroupAccess;
import com.group3.money.entity.DGroup;
import com.group3.money.views.IFormView;

public class GroupFormPresenter{
    private IFormView iFormView;
    private DGroupAccess dGroupAccess;

    public GroupFormPresenter(IFormView iFormView, Context context) {
        this.iFormView = iFormView;
        dGroupAccess = AppDatabase.getInstance(context).dGroupAccess();
    }

    public void insert(final DGroup group){
        if(group.getName() == null){
            iFormView.showError("Tên nhóm chưa đk nhập");
        }
        else{
            new Thread(new Runnable() {
                @Override
                public void run() {
                    dGroupAccess.insert(group);
                    iFormView.navigateBack(null);
                }
            }).start();
        }
    }

}
