package com.group3.money.presenters;

import android.content.Context;

import com.group3.money.dao.AppDatabase;
import com.group3.money.dao.DCategoryAccess;
import com.group3.money.dao.VCategoryAccess;
import com.group3.money.entity.DCategory;
import com.group3.money.entity.VCategory;
import com.group3.money.views.IListView;

import java.util.LinkedList;
import java.util.List;

public class CategoryListPresenter {
    private IListView iListView;
    private DCategoryAccess dCategoryAccess;
    private VCategoryAccess vCategoryAccess;

    public CategoryListPresenter(IListView iListView, Context context) {
        this.iListView = iListView;
        AppDatabase appDatabase = AppDatabase.getInstance(context);
        dCategoryAccess = appDatabase.dCategoryAccess();
        vCategoryAccess = appDatabase.vCategoryAccess();
    }

    public void getByType(final String groupId, final int i) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                ;
                List<VCategory> categoryRoots = vCategoryAccess.getRootByType(groupId,i);
                List<VCategory> categoryList = new LinkedList<>();
                for (VCategory v: categoryRoots) {
                    categoryList.add(v);
                    List<VCategory> categoryChildren = vCategoryAccess.getByRootId(groupId,v.getId());
                    if(categoryChildren.isEmpty()) continue;
                    categoryList.addAll(categoryChildren);
                }
                iListView.showList(categoryList);
            }
        }).start();


    }

    public void create(DCategory category) {
        try {
            dCategoryAccess.insert(category);
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}
