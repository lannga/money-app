package com.group3.money.presenters;

import android.content.Context;

import com.group3.money.dao.AppDatabase;
import com.group3.money.dao.DTransactionAccess;
import com.group3.money.dao.VCategoryAccess;
import com.group3.money.dao.VTransactionAccess;
import com.group3.money.entity.DTransaction;
import com.group3.money.entity.VCategory;
import com.group3.money.entity.VTransaction;
import com.group3.money.views.IListView;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class TransactionListPresenter {
    private IListView iListView;
    private VTransactionAccess vTransactionAccess;
    private DTransactionAccess dTransactionAccess;
    private VCategoryAccess vCategoryAccess;

    public TransactionListPresenter(IListView iListView, Context context) {
        this.iListView = iListView;
        AppDatabase appDatabasea = AppDatabase.getInstance(context);
        vTransactionAccess = appDatabasea.vTransactionAccess();
        dTransactionAccess = appDatabasea.dTransactionAccess();
        vCategoryAccess = appDatabasea.vCategoryAccess();
    }

    public void getList(final String groupId, final VCategory category, final Calendar fromTime, final Calendar toTime, final int type) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<VTransaction> list = new LinkedList<>();
                if (category == null && type == 0) {
                    list = vTransactionAccess.getList(groupId, fromTime.getTimeInMillis(), toTime.getTimeInMillis());
                } else if (category == null && type != 0) {
                    list = vTransactionAccess.getListByType(groupId, type, fromTime.getTimeInMillis(), toTime.getTimeInMillis());
                } else if (category != null && type == 0) {
                    list = vTransactionAccess.getListByCat(groupId,category.getId(), fromTime.getTimeInMillis(), toTime.getTimeInMillis());
                } else {
                    list = vTransactionAccess.getListByTypeCat(groupId,category.getId(), fromTime.getTimeInMillis(), toTime.getTimeInMillis(), type);
                }
                iListView.showList(list);
            }
        }).

                start();

    }

    public void delete(final DTransaction dTransaction){
        new Thread(new Runnable() {
            @Override
            public void run() {
                dTransactionAccess.delete(dTransaction);
            }
        }).start();
    }

    public List<VCategory> getCategories(String groupId, int i) {
        List<VCategory> categoryRoots = (i == 0) ? vCategoryAccess.getRoot(groupId) : vCategoryAccess.getRootByType(groupId,i);
        List<VCategory> categoryList = new LinkedList<>();
        for (VCategory v : categoryRoots) {
            categoryList.add(v);
            List<VCategory> categoryChildren = vCategoryAccess.getByRootId(groupId,v.getId());
            if (categoryChildren.isEmpty()) continue;
            categoryList.addAll(categoryChildren);
        }
        return categoryList;
    }
}
