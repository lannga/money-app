package com.group3.money.utils;


import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.group3.money.R;

public class Bar{



    public static void createBar(final AppCompatActivity appCompatActivity, String title){
        Toolbar toolbar = appCompatActivity.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_white);
        appCompatActivity.setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appCompatActivity.finish();
            }
        });
        TextView textView = toolbar.findViewById(R.id.toolbar_title);
        textView.setText(title);
        appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
