package com.group3.money.utils;

import com.group3.money.R;

public class Constants {

    public static final int EXPENSE = 1;
    public static final int INCOME = 2;

    public static final int DAY = 0;
    public static final int MONTH = 1;
    public static final int YEAR = 2;

    public static final String[] TIME_PATTERN ={"dd/MM/yyyy","MM/yyyy","yyyy"};
    public static final String[] type1 ={"Chi", "Thu"};
    public static final String[] type = {"Tất", "Chi", "Thu"};
    public static final int[] CAT_ICONS = {
            R.drawable.ic_cat_beach_access,
            R.drawable.ic_cat_default,
            R.drawable.ic_cat_business_center,
            R.drawable.ic_cat_cake,
            R.drawable.ic_cat_child_friendly,
            R.drawable.ic_cat_directions_bus,
            R.drawable.ic_cat_email,
            R.drawable.ic_cat_enu_book,
            R.drawable.ic_cat_fastfood,
            R.drawable.ic_cat_favorite,
            R.drawable.ic_cat_fitness_center,
            R.drawable.ic_cat_flight,
            R.drawable.ic_cat_format_paint,
            R.drawable.ic_cat_headset_mic,
            R.drawable.ic_cat_house,
            R.drawable.ic_cat_laptop,
            R.drawable.ic_cat_live_tv,
            R.drawable.ic_cat_local_dining,
            R.drawable.ic_cat_local_gas_station,
            R.drawable.ic_cat_local_grocery_store,
            R.drawable.ic_cat_local_mall,
            R.drawable.ic_cat_local_movies,
            R.drawable.ic_cat_local_offer,
            R.drawable.ic_cat_local_phone,
            R.drawable.ic_cat_menu_book,
            R.drawable.ic_cat_music_note,
            R.drawable.ic_cat_opacity,
            R.drawable.ic_cat_pets,
            R.drawable.ic_cat_power,
            R.drawable.ic_cat_school,
            R.drawable.ic_cat_sim_card,
            R.drawable.ic_cat_sports_esports,
            R.drawable.ic_cat_sports_football,
            R.drawable.ic_cat_square_foot,
            R.drawable.ic_cat_watch,
    };

    public static final int[] GROUP_ICONS ={
            R.drawable.ic_gr_deck,
            R.drawable.ic_gr_fireplace,
            R.drawable.ic_gr_nights_stay,
            R.drawable.ic_gr_outdoor_grill,
            R.drawable.ic_gr_photo_camera,
            R.drawable.ic_gr_radio,
            R.drawable.ic_gr_restaurant,
            R.drawable.ic_gr_sports,
            R.drawable.ic_group
    };

}
