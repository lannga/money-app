package com.group3.money.utils;

import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.group3.money.entity.DGroup;

public class GroupPreference {

    public static DGroup getCurentGroup(AppCompatActivity appCompatActivity){
        SharedPreferences sharedPreferences = appCompatActivity.getSharedPreferences("Pref", 0);
        DGroup dGroup = new Gson().fromJson(sharedPreferences.getString("group",null), DGroup.class);
        return dGroup;
    }

    public static void setGroup(AppCompatActivity appCompatActivity, DGroup dGroup){
        SharedPreferences sharedPreferences = appCompatActivity.getSharedPreferences("Pref", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String strDGroup = new Gson().toJson(dGroup);
        editor.putString("group", strDGroup);
        editor.apply();
    }
}
