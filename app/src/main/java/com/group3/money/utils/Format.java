package com.group3.money.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Format {

    public static String stringDate(Calendar calendar, String pattern){
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(calendar.getTime());
    }

    public static String stringDate(long time, String pattern){
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(time);
    }
    public static long[] getFromAndTo(Calendar calendar, int index){
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(calendar.getTimeInMillis()));
        long fromTime = 0, toTime = 0;
        switch (index){
            case Constants.DAY:
                setStartOfDay(c);
                fromTime = c.getTimeInMillis();
                setLastOfDay(c);
                toTime = c.getTimeInMillis();
                break;
            case Constants.MONTH:
                c.set(Calendar.DAY_OF_MONTH, 1);
                setStartOfDay(c);
                fromTime = c.getTimeInMillis();
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                setLastOfDay(c);
                toTime = c.getTimeInMillis();
                break;
            case Constants.YEAR:
                c.set(Calendar.DAY_OF_YEAR, 1);
                fromTime = c.getTimeInMillis();
                c.add(Calendar.DAY_OF_YEAR, c.getActualMaximum(Calendar.DAY_OF_YEAR));
                setLastOfDay(c);
                toTime = c.getTimeInMillis();
        }
        long[] time = {fromTime, toTime};
        return time;
    }

    public static void setStartOfDay(Calendar c){
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
    }

    public static void setLastOfDay(Calendar c){
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
    }


    public static String formatMoney(int money){
        return null;
    }
}
