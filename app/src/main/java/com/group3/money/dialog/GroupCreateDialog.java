package com.group3.money.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.group3.money.R;
import com.group3.money.adapters.IconAdapter;
import com.group3.money.adapters.IconHorizonalAdapter;
import com.group3.money.entity.DGroup;
import com.group3.money.presenters.GroupFormPresenter;
import com.group3.money.presenters.GroupListPresenter;
import com.group3.money.utils.Constants;
import com.group3.money.views.IChoseIconView;
import com.group3.money.views.IFormView;


public class GroupCreateDialog extends Dialog implements View.OnClickListener, TextWatcher, IFormView, IChoseIconView{

    private TextView name, error;
    
    private Button cancel, save;

    private RecyclerView recyclerView;

    private IconHorizonalAdapter iconAdapter;
    
    private DGroup dGroup;

    private GroupFormPresenter groupFormPresenter;

    private GroupListPresenter groupListPresenter;


    public GroupCreateDialog(@NonNull Context context, GroupListPresenter groupListPresenter) {
        super(context);
        this.groupListPresenter = groupListPresenter;
        
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_group);
        name = findViewById(R.id.name_edt);
        
        dGroup = new DGroup(R.drawable.ic_group);
        
        cancel = findViewById(R.id.cancel_btn);
        save = findViewById(R.id.save_btn);

        recyclerView = findViewById(R.id.icon_rv);

        iconAdapter = new IconHorizonalAdapter(Constants.GROUP_ICONS, this, getContext());

        recyclerView.setAdapter(iconAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        error = findViewById(R.id.error_tv);
        cancel.setOnClickListener(this);
        save.setOnClickListener(this);
        name.addTextChangedListener(this);
        groupFormPresenter = new GroupFormPresenter(this, getContext());
        
    }

    @Override
    public void onClick(View view) {
        if(view == cancel){
            dismiss();
        }else{
            groupFormPresenter.insert(dGroup);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        dGroup.setName(charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void showError(String message) {
        error.setVisibility(View.VISIBLE);
        error.setText(message);
    }

    @Override
    public void navigateBack(Object obj) {
        dismiss();
        groupListPresenter.getList();
    }

    @Override
    public void choseIcon(int i) {
        dGroup.setIcon(i);
    }
}
