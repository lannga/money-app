package com.group3.money.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import com.group3.money.views.IDeleteItemView;
import com.group3.money.views.IListView;

public class DeleteDialog extends DialogFragment {
    private IDeleteItemView iDeleteItemView;
    public DeleteDialog(IDeleteItemView iDeleteItemView) {
        this.iDeleteItemView = iDeleteItemView;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Xóa ???")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        iDeleteItemView.confirmDelete();
                    }
                })
                .setNegativeButton("Cancel;", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        iDeleteItemView.cancelDelete();
                    }
                });
        return builder.create();
    }
}
