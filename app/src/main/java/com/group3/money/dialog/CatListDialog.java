package com.group3.money.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.group3.money.R;
import com.group3.money.views.ICreateCategoryView;

public class CatListDialog extends Dialog implements View.OnClickListener {
    private Context context;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private Button createBtn;
    private ICreateCategoryView iCreateCategoryView;

    private int layout, recycler;

    protected CatListDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public CatListDialog(@NonNull Context context, RecyclerView.Adapter adapter, int layout, int recycler, ICreateCategoryView iCreateCategoryView) {
        super(context);
        this.context = context;
        this.adapter = adapter;
        this.layout = layout;
        this.recycler = recycler;
        this.iCreateCategoryView = iCreateCategoryView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout);
        createBtn = findViewById(R.id.cat_create_btn);
        recyclerView = findViewById(recycler);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        createBtn.setOnClickListener(this);
        if(iCreateCategoryView == null){
            createBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        iCreateCategoryView.goToCategoryForm();
    }

}
