package com.group3.money.dialog;

import androidx.fragment.app.DialogFragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.group3.money.R;
import com.group3.money.utils.Constants;
import com.group3.money.views.IChoseTypeView;

public class TypeDialog extends DialogFragment implements DialogInterface.OnClickListener {
    private IChoseTypeView iChoseTypeView;
    private String[] typeList;

    public TypeDialog(IChoseTypeView iChoseTypeView, String[] typeList) {
        this.iChoseTypeView = iChoseTypeView;
        this.typeList = typeList;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Chọn loại giao dịch")
                .setItems(typeList, this);
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        i = (typeList == Constants.type1 )? (i+1) : i;
        iChoseTypeView.choseType(i);
    }
}
