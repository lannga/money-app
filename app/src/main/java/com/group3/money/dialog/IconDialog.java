package com.group3.money.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.group3.money.R;
import com.group3.money.adapters.IconAdapter;
import com.group3.money.views.IChoseIconView;

public class IconDialog extends Dialog {
    private Context context;
    private RecyclerView recyclerView;
    private IconAdapter adapter;

    private int[] list;

    protected IconDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public IconDialog(@NonNull Context context, int[] list, IChoseIconView iChoseIconView) {
        super(context);
        this.context = context;
        this.list = list;
        adapter = new IconAdapter(list, iChoseIconView);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_icon);
        recyclerView = findViewById(R.id.icon_rv);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(context,3));
    }
}
