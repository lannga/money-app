package com.group3.money.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.group3.money.R;
import com.group3.money.adapters.CatTypePagerAdapter;
import com.group3.money.adapters.CategoryAdapter;
import com.group3.money.adapters.TimePagerAdapter;
import com.group3.money.entity.VCategory;
import com.group3.money.presenters.CategoryListPresenter;
import com.group3.money.utils.Bar;
import com.group3.money.utils.Constants;
import com.group3.money.views.IListView;

import java.util.LinkedList;
import java.util.List;

public class CategoryListActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, View.OnClickListener {

    private Button categoryCreateButton;


    private TabLayout catTypeTabLayout;
    private ViewPager viewPager;

    private int type = Constants.EXPENSE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);

        Bar.createBar(this, "Category");

        catTypeTabLayout = findViewById(R.id.catType_tabLayout);
        viewPager = findViewById(R.id.viewPager);


        categoryCreateButton = findViewById(R.id.cat_create_btn);
        categoryCreateButton.setOnClickListener(this);

        catTypeTabLayout.addTab(catTypeTabLayout.newTab().setText(Constants.type[Constants.EXPENSE]));
        catTypeTabLayout.addTab(catTypeTabLayout.newTab().setText(Constants.type[Constants.INCOME]));

        CatTypePagerAdapter catTypePagerAdapterPagerAdapter = new CatTypePagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(catTypePagerAdapterPagerAdapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(catTypeTabLayout));

        catTypeTabLayout.addOnTabSelectedListener(this);
    }


    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, CategoryFormActivity.class);
        intent.putExtra("type", type);
        startActivity(intent);
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        type = tab.getPosition() +1 ;
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
