package com.group3.money.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.group3.money.R;
import com.group3.money.adapters.Base1Adapter;
import com.group3.money.dialog.CatListDialog;
import com.group3.money.dialog.DateDialog;
import com.group3.money.dialog.TypeDialog;
import com.group3.money.entity.DCategory;
import com.group3.money.entity.DGroup;
import com.group3.money.entity.DTransaction;
import com.group3.money.entity.VCategory;
import com.group3.money.entity.VTransaction;
import com.group3.money.presenters.TransactionFormPresenter;
import com.group3.money.utils.Bar;
import com.group3.money.utils.Constants;
import com.group3.money.utils.Format;
import com.group3.money.utils.GroupPreference;
import com.group3.money.views.IChoseItemView;
import com.group3.money.views.IChoseDateView;
import com.group3.money.views.IChoseTypeView;
import com.group3.money.views.ICreateCategoryView;
import com.group3.money.views.IFormView;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class TransactionFormActivity extends AppCompatActivity implements
        IFormView, IChoseTypeView, IChoseDateView, IChoseItemView, ICreateCategoryView,
        View.OnClickListener, TextWatcher,
        Runnable {

    private Button saveBtn;
    protected Calendar calendar = Calendar.getInstance();

    private EditText amountEditText, noteEditText, time, type, catEditText;
    protected static DTransaction transaction;
    private TransactionFormPresenter transactionFormPresenter;

    private DialogFragment typeDialog, dateDialog;
    private CatListDialog catListDialog;

    private Base1Adapter base1Adapter;

    private List<VCategory> categories;

    private DGroup currentGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_form);

        currentGroup = GroupPreference.getCurentGroup(this);

        Bar.createBar(this,"Tạo giao dich");

        transactionFormPresenter = new TransactionFormPresenter(this, this);
        categories = new LinkedList<>();
        transaction = new DTransaction(currentGroup.getId(), Constants.EXPENSE, calendar.getTimeInMillis());
        base1Adapter = new Base1Adapter(categories, this);

        typeDialog = new TypeDialog(this, Constants.type1);
        dateDialog = new DateDialog(this, calendar);
        catListDialog = new CatListDialog(this, base1Adapter, R.layout.list_category, R.id.root_cat_rcv, this);

        time = findViewById(R.id.time_chose);
        type = findViewById(R.id.type_chose);
        amountEditText = findViewById(R.id.amount_input);
        noteEditText = findViewById(R.id.note_input);
        catEditText = findViewById(R.id.category_chose);
        saveBtn = findViewById(R.id.save_btn);

        time.setText(Format.stringDate(calendar, Constants.TIME_PATTERN[0]));
        type.setText(Constants.type[Constants.EXPENSE]);

        time.setOnClickListener(this);
        type.setOnClickListener(this);
        catEditText.setOnClickListener(this);
        amountEditText.addTextChangedListener(this);
        noteEditText.addTextChangedListener(this);
        saveBtn.setOnClickListener(this);

        fillInfo();
    }

    public void fillInfo(){
         VTransaction vTransaction =(VTransaction) getIntent().getSerializableExtra("vTransaction");
         if(vTransaction == null) return;
         transaction = vTransaction;
         type.setText(Constants.type[vTransaction.getType()]);
         catEditText.setText(vTransaction.getCategoryName());
         amountEditText.setText(""+vTransaction.getAmount());
         noteEditText.setText(vTransaction.getNote());
         time.setText(Format.stringDate(vTransaction.getTime(), Constants.TIME_PATTERN[0]));
    }

    @Override
    protected void onStart() {
        super.onStart();
        new Thread(this).start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            DCategory dCategory = (DCategory) data.getSerializableExtra("dCategory");
            transaction.setCategoryId(dCategory.getId());
            catEditText.setText(dCategory.getName());
            catListDialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {

        if (view != noteEditText && view != amountEditText) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            noteEditText.clearFocus();
            amountEditText.clearFocus();
        }
        switch (view.getId()) {
            case R.id.type_chose:
                typeDialog.show(getSupportFragmentManager(), "type-chose");
                break;
            case R.id.category_chose:
                catListDialog.show();
                catListDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                break;
            case R.id.time_chose:
                dateDialog.show(getSupportFragmentManager(), "datePicker");
                break;
            case R.id.save_btn:
                transactionFormPresenter.insert(transaction);
                break;
        }
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (noteEditText.hasFocus()) {
            transaction.setNote(charSequence.toString());
        } else if (amountEditText.hasFocus()) {
            int amount = charSequence.length() > 0 ? Integer.parseInt(charSequence.toString()) : 0;
            transaction.setAmount(amount);
        }
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateBack(Object obj) {
        finish();
    }

    @Override
    public void choseType(int i) {
        transaction.setType(i);
        type.setText(Constants.type[i]);
        catEditText.setText("");
        transaction.setCategoryId(null);
        new Thread(this).start();
    }

    @Override
    public void choseDate(Calendar c) {
        calendar = c;
        time.setText(Format.stringDate(calendar,Constants.TIME_PATTERN[0]));
        transaction.setTime(calendar.getTimeInMillis());

    }

    @Override
    public void choseItem(Object obj) {
        VCategory cat = (VCategory) obj;
        catListDialog.dismiss();
        transaction.setCategoryId(cat.getId());
        catEditText.setText(cat.getName());
    }

    @Override
    public void run() {
        categories.clear();
        categories.addAll(transactionFormPresenter.getCategories(currentGroup.getId(), transaction.getType()));
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                base1Adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void goToCategoryForm() {
        Intent i = new Intent(this, CategoryFormActivity.class);
        i.putExtra("type", transaction.getType());
        startActivityForResult(i, 1);
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

}
