package com.group3.money.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.gson.Gson;
import com.group3.money.R;
import com.group3.money.dao.AppDatabase;
import com.group3.money.entity.DGroup;
import com.group3.money.utils.GroupPreference;

import java.util.List;

public class WelcomeActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        final Intent intent = new Intent(this, MainActivity.class);
        if (GroupPreference.getCurentGroup(this) == null) {

            final DGroup dGroup = new DGroup("0", "Cá Nhân", R.drawable.ic_group);
            GroupPreference.setGroup(this, dGroup);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    AppDatabase appDatabase = AppDatabase.getInstance(getApplication());
                    appDatabase.dGroupAccess().insert(dGroup);
                    startActivity(intent);
                    finish();
                }
            }).start();
            return;
        }
        startActivity(intent);
        finish();
    }
}
