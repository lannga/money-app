package com.group3.money.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.google.gson.Gson;
import com.group3.money.R;
import com.group3.money.adapters.Base1Adapter;
import com.group3.money.adapters.GroupAdapter;
import com.group3.money.dialog.GroupCreateDialog;
import com.group3.money.entity.DGroup;
import com.group3.money.presenters.GroupListPresenter;
import com.group3.money.utils.Bar;
import com.group3.money.utils.GroupPreference;
import com.group3.money.views.IChoseItemView;
import com.group3.money.views.IListView;

import java.util.LinkedList;
import java.util.List;

public class GroupListActivity extends AppCompatActivity implements IListView, IChoseItemView,
        View.OnClickListener {

    private Button groupCreateBtn;
    private RecyclerView groupRecyclerView;
    private GroupAdapter groupAdapter;

    private GroupListPresenter groupListPresenter;

    private List<DGroup> dGroups;

    private GroupCreateDialog groupCreateDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_list);
        Bar.createBar(this, "Nhóm");

        dGroups = new LinkedList<>();
        groupListPresenter = new GroupListPresenter(this, this);
        groupAdapter = new GroupAdapter(dGroups, this);

        groupRecyclerView = findViewById(R.id.group_rcv);
        groupRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        groupRecyclerView.setAdapter(groupAdapter);

        groupCreateBtn = findViewById(R.id.group_create_btn);
        groupCreateBtn.setOnClickListener(this);

        groupCreateDialog = new GroupCreateDialog(this, groupListPresenter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        groupListPresenter.getList();
    }

    @Override
    public void onClick(View view) {
        groupCreateDialog.show();;
    }

    @Override
    public void showList(Object... list) {
        List<DGroup> groups = (List<DGroup>) list[0];
        dGroups.clear();
        dGroups.addAll(groups);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                groupAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void choseItem(Object obj) {
        GroupPreference.setGroup(this, (DGroup) obj);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finishAffinity();
    }
}
