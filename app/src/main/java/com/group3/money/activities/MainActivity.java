package com.group3.money.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.group3.money.R;
import com.group3.money.adapters.TimePagerAdapter;
import com.group3.money.entity.DGroup;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, View.OnClickListener {

    private TabLayout timeTabLayout;
    private ViewPager viewPager;
    private LinearLayout transactionCreateBtn, categoryBtn, planBtn, groupBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences("Pref", 0);

        DGroup dGroup = new Gson().fromJson(sharedPreferences.getString("group",null), DGroup.class);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView textView = toolbar.findViewById(R.id.toolbar_title);
        textView.setText(dGroup.getName());
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        timeTabLayout = findViewById(R.id.time_tabLayout);
        viewPager = findViewById(R.id.viewPager);
        transactionCreateBtn = findViewById(R.id.trans_create_btn);
        categoryBtn = findViewById(R.id.cat_btn);
        planBtn = findViewById(R.id.cat_btn);
        groupBtn = findViewById(R.id.group_btn);

        transactionCreateBtn.setOnClickListener(this);
        categoryBtn.setOnClickListener(this);
        planBtn.setOnClickListener(this);
        groupBtn.setOnClickListener(this);

        timeTabLayout.addTab(timeTabLayout.newTab().setText("Ngày"));
        timeTabLayout.addTab(timeTabLayout.newTab().setText("Tháng"));
        timeTabLayout.addTab(timeTabLayout.newTab().setText("Năm"));

        final TimePagerAdapter timePagerAdapter = new TimePagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(timePagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(timeTabLayout));
        timeTabLayout.addOnTabSelectedListener(this);
        timeTabLayout.getTabAt(1).select();

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.trans_create_btn:
                intent = new Intent(this, TransactionFormActivity.class);
                break;
            case R.id.cat_btn:
                intent = new Intent(this, CategoryListActivity.class);
                break;
            case R.id.group_btn:
                intent = new Intent(this, GroupListActivity.class);
                break;
            case R.id.plan_btn:
                break;
            default:
                return;
        }
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.trans_list_btn:
                Intent intent= new Intent(this, TransactionListActivity.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

}
