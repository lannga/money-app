package com.group3.money.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.group3.money.R;
import com.group3.money.adapters.Base1Adapter;
import com.group3.money.adapters.CategoryAdapter;
import com.group3.money.adapters.SwipeController;
import com.group3.money.adapters.TransactionAdapter;
import com.group3.money.dialog.CatListDialog;
import com.group3.money.dialog.DateDialog;
import com.group3.money.dialog.DeleteDialog;
import com.group3.money.dialog.TypeDialog;
import com.group3.money.entity.DCategory;
import com.group3.money.entity.DGroup;
import com.group3.money.entity.DTransaction;
import com.group3.money.entity.VCategory;
import com.group3.money.entity.VTransaction;
import com.group3.money.presenters.TransactionListPresenter;
import com.group3.money.utils.Bar;
import com.group3.money.utils.Constants;
import com.group3.money.utils.Format;
import com.group3.money.utils.GroupPreference;
import com.group3.money.views.IChoseDateView;
import com.group3.money.views.IChoseItemView;
import com.group3.money.views.IChoseTypeView;
import com.group3.money.views.ICreateCategoryView;
import com.group3.money.views.IDeleteItemView;
import com.group3.money.views.IListView;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class TransactionListActivity extends AppCompatActivity implements View.OnClickListener,
        IListView, IChoseTypeView, IChoseDateView, IChoseItemView, IDeleteItemView,
        Runnable {

    private RecyclerView transactionRcV;
    private TransactionAdapter transactionAdapter;

    private Base1Adapter categoryAdapter;
    private List<VCategory> categories;
    private List<VTransaction> vTransactions;
    private Button searchBtn;

    private LinearLayout formLnL;

    private TransactionListPresenter transactionListPresenter;

    private TypeDialog typeDialog;
    private DateDialog dateDialog;
    private CatListDialog catListDialog;
    private DeleteDialog deleteDialog;

    private EditText typeEdt, fromTimeEdt, toTimeEdt, catEdt;

    private Calendar fromTime, toTime;

    private int type = 0;
    private VCategory category;

    private VTransaction vTransaction;

    private DGroup currentGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_list);

        Bar.createBar(this,"Danh sách giao dịch");
        currentGroup = GroupPreference.getCurentGroup(this);

        transactionListPresenter = new TransactionListPresenter(this, this);

        fromTime = Calendar.getInstance();
        toTime = Calendar.getInstance();
        fromTime.set(Calendar.DAY_OF_MONTH,1);
        Format.setStartOfDay(fromTime);
        Format.setLastOfDay(toTime);

        formLnL = findViewById(R.id.filter_form_lnl);
        formLnL.setVisibility(View.GONE);

        typeEdt = findViewById(R.id.type_edt);
        catEdt = findViewById(R.id.cat_edt);
        fromTimeEdt = findViewById(R.id.from_time_edt);
        toTimeEdt = findViewById(R.id.to_time_edt);

        fromTimeEdt.setText(Format.stringDate(fromTime, Constants.TIME_PATTERN[0]));
        toTimeEdt.setText(Format.stringDate(toTime, Constants.TIME_PATTERN[0]));
        typeEdt.setOnClickListener(this);
        catEdt.setOnClickListener(this);
        fromTimeEdt.setOnClickListener(this);
        toTimeEdt.setOnClickListener(this);

        typeDialog = new TypeDialog(this, Constants.type);
        dateDialog = new DateDialog(this, fromTime);
        deleteDialog = new DeleteDialog(this);
        categories = new LinkedList<>();
        categoryAdapter = new Base1Adapter(categories, this);
        catListDialog = new CatListDialog(this, categoryAdapter, R.layout.list_category, R.id.root_cat_rcv, null);

        searchBtn = findViewById(R.id.search_btn);

        transactionRcV = findViewById(R.id.trans_rcv);
        transactionRcV.setLayoutManager(new LinearLayoutManager(this));
        vTransactions = new LinkedList<>();
        transactionAdapter = new TransactionAdapter(vTransactions, this);
        transactionRcV.setAdapter(transactionAdapter);

        searchBtn.setOnClickListener(this);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeController(categoryAdapter, this));
        itemTouchHelper.attachToRecyclerView(transactionRcV);
    }

    @Override
    protected void onStart() {
        super.onStart();
        transactionListPresenter.getList(currentGroup.getId(), category,fromTime, toTime, type);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter_btn:
                if (formLnL.getVisibility() == View.GONE) {
                    formLnL.setVisibility(View.VISIBLE);
                } else {
                    formLnL.setVisibility(View.GONE);
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showList(Object... list) {
        vTransactions.clear();
        vTransactions.addAll((List<VTransaction>) list[0]);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                transactionAdapter.notifyDataSetChanged();
            }
        });
    }

//    @Override
//    public void changeItem(Object item) {
//        Intent intent = new Intent(this, TransactionFormActivity.class);
//        intent.putExtra("vTransaction", (VTransaction) item);
//        startActivityForResult(intent, 2);
//    }

//    @Override
//    public void getDeleteItem(Object item) {
//        deleteDialog.show(getSupportFragmentManager(),"delete-dialog");
//        vTransaction = (VTransaction) item;
//
//    }

    @Override
    public void choseType(int i) {
        type = i;
        typeEdt.setText(Constants.type[i]);
        category = null;
        catEdt.setText("");
        new Thread(this).start();
    }

    @Override
    public void choseDate(Calendar calendar) {
        fromTimeEdt.setText(Format.stringDate(fromTime, Constants.TIME_PATTERN[0]));
        toTimeEdt.setText(Format.stringDate(toTime, Constants.TIME_PATTERN[0]));
        if(calendar.equals(toTime)){
            Format.setLastOfDay(toTime);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.type_edt:
                typeDialog.show(getSupportFragmentManager(), "type_dialog");
                break;
            case R.id.cat_edt:
                catListDialog.show();
                break;
            case R.id.from_time_edt:
                dateDialog.setCalendar(fromTime);
                dateDialog.show(getSupportFragmentManager(), "date-dialog");
                break;
            case R.id.to_time_edt:
                dateDialog.setCalendar(toTime);
                dateDialog.show(getSupportFragmentManager(), "date-dialog");
                break;
            case R.id.search_btn:
                transactionListPresenter.getList(currentGroup.getId(),category,fromTime,toTime,type);
        }
    }

    @Override
    public void choseItem(Object obj) {
        catListDialog.dismiss();
        category = (VCategory) obj;
        catEdt.setText(category.getName());
    }

    @Override
    public void showNotification(int i) {
        deleteDialog.show(getSupportFragmentManager(), null);
        vTransaction = vTransactions.get(i);
    }

    @Override
    public void confirmDelete() {
        vTransactions.remove(vTransaction);
        transactionAdapter.notifyDataSetChanged();
        transactionListPresenter.delete(vTransaction);
    }

    @Override
    public void cancelDelete() {
        transactionAdapter.notifyDataSetChanged();
    }

    @Override
    public void run() {
        categories.clear();
        categories.addAll(transactionListPresenter.getCategories(currentGroup.getId(),type));
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                categoryAdapter.notifyDataSetChanged();
            }
        });
    }
}
