package com.group3.money.activities;



import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import android.view.View;

import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.group3.money.R;
import com.group3.money.adapters.Base1Adapter;

import com.group3.money.dialog.CatListDialog;
import com.group3.money.dialog.IconDialog;
import com.group3.money.dialog.TypeDialog;
import com.group3.money.entity.DCategory;
import com.group3.money.entity.DGroup;
import com.group3.money.entity.VCategory;
import com.group3.money.presenters.CategoryFormPresenter;
import com.group3.money.utils.Bar;
import com.group3.money.utils.Constants;
import com.group3.money.utils.GroupPreference;
import com.group3.money.views.IChoseItemView;
import com.group3.money.views.IChoseIconView;
import com.group3.money.views.IChoseTypeView;
import com.group3.money.views.IFormView;

import java.util.LinkedList;
import java.util.List;

public class CategoryFormActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher,
        Runnable,
        IChoseIconView, IChoseItemView, IFormView, IChoseTypeView {

    private EditText name, catRootEdt, typeEdt;
    private DCategory category;
    private ImageView iconImageView;
    private Button saveBtn;

    private static CategoryFormPresenter categoryFormPresenter;
    private IconDialog iconDialog;
    private TypeDialog typeDialog;

    private Base1Adapter base1Adapter;

    private CatListDialog rootCatDialog;

    private List<VCategory> categories;

    private DGroup currentGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_form);
        Bar.createBar(this,"Tạo category");

        currentGroup = GroupPreference.getCurentGroup(this);

        categoryFormPresenter = new CategoryFormPresenter(this, this);

        int intentType = getIntent().getIntExtra("type", 0);
        int type = intentType == 0 ? Constants.EXPENSE : intentType;

        category = new DCategory(currentGroup.getId(), type);
        category.setIcon(R.drawable.ic_category);
        categories = new LinkedList<>();

        base1Adapter = new Base1Adapter(categories, this);

        iconDialog = new IconDialog(this, Constants.CAT_ICONS, this);
        rootCatDialog = new CatListDialog(this, base1Adapter, R.layout.list_category, R.id.root_cat_rcv, null);
        typeDialog = new TypeDialog(this, Constants.type1);

        name = findViewById(R.id.name);
        iconImageView = findViewById(R.id.ic_cat);
        catRootEdt = findViewById(R.id.cat_root_edt);
        typeEdt = findViewById(R.id.type_edt);
        saveBtn = findViewById(R.id.save_btn);

        typeEdt.setText(Constants.type[type]);

        saveBtn.setOnClickListener(this);
        name.addTextChangedListener(this);
        catRootEdt.setOnClickListener(this);
        iconImageView.setOnClickListener(this);
        typeEdt.setOnClickListener(this);

        new Thread(this).start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_btn:
                categoryFormPresenter.create(category);
                break;
            case R.id.ic_cat:
                iconDialog.show();
                break;
            case R.id.cat_root_edt:
                rootCatDialog.show();
                rootCatDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                break;
            case R.id.type_edt:
                typeDialog.show(getSupportFragmentManager(), "typeChose");
            default:
                break;
        }
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.length() != 0) {
            category.setName(charSequence.toString());
        }
    }

    @Override
    public void choseIcon(int i) {
        category.setIcon(i);
        iconImageView.setImageResource(i);
        iconDialog.dismiss();
    }

    @Override
    public void run() {
        categories.clear();
        categories.addAll(categoryFormPresenter.getRootByType(currentGroup.getId(), category.getType()));
        base1Adapter.notifyDataSetChanged();
    }

    @Override
    public void choseItem(Object c) {
        VCategory cat = (VCategory) c;
        category.setRootId(cat.getId());
        catRootEdt.setText(cat.getName());
        rootCatDialog.dismiss();
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void choseType(int i) {
        category.setType(i);
        typeEdt.setText(Constants.type[i]);
        new Thread(this).start();
    }

    @Override
    public void navigateBack(Object obj) {
        Intent intent = new Intent();
        intent.putExtra("dCategory",(DCategory) obj);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void afterTextChanged(Editable editable) {}
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

}
