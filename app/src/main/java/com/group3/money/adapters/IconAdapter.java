package com.group3.money.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group3.money.R;
import com.group3.money.views.IChoseIconView;

public class IconAdapter extends RecyclerView.Adapter<IconAdapter.ViewHolder> {

    private int[] icons;
    private IChoseIconView iView;
    public IconAdapter(int[] icons, IChoseIconView iView) {
        this.icons = icons;
        this.iView = iView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_icon, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.imageView.setImageResource(icons[position]);
    }

    @Override
    public int getItemCount() {
        return icons.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imageView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.icon_area);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view){
            int position = getLayoutPosition();
            iView.choseIcon(icons[position]);

        }
    }
}


