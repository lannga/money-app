package com.group3.money.adapters;



import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.group3.money.fragments.CatTypeFragment;
import com.group3.money.fragments.StatisticFragment;
import com.group3.money.utils.Constants;

public class CatTypePagerAdapter extends FragmentPagerAdapter {
    private AppCompatActivity appCompatActivity;
    public CatTypePagerAdapter(FragmentManager fm, AppCompatActivity appCompatActivity) {
        super(fm);
        this.appCompatActivity = appCompatActivity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new CatTypeFragment(Constants.EXPENSE, appCompatActivity);
            case 1:
                return new CatTypeFragment(Constants.INCOME, appCompatActivity);
            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return 2;
    }
}
