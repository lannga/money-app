package com.group3.money.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.group3.money.R;
import com.group3.money.views.IChoseIconView;

public class IconHorizonalAdapter extends RecyclerView.Adapter<IconHorizonalAdapter.ViewHolder> {

    private int[] icons;
    private int chosedPosition = 0;
    private View a = null;
    private IChoseIconView iView;

    private Context context;

    public IconHorizonalAdapter(int[] icons, IChoseIconView iView, Context context) {
        this.icons = icons;
        this.iView = iView;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_icon_hor, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == chosedPosition) {
            a = holder.imageView;
            holder.imageView.setBackground(context.getResources().getDrawable(R.drawable.round_retangle_green_stroke));
        } else {
            holder.imageView.setBackgroundColor(0);
        }
        holder.imageView.setImageResource(icons[position]);
    }

    @Override
    public int getItemCount() {
        return icons.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.icon_area);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getLayoutPosition();
            chosedPosition = position;
            a.setBackgroundColor(0);
            view.setBackground(context.getResources().getDrawable(R.drawable.round_retangle_green_stroke));
            a = view;
            iView.choseIcon(icons[position]);
        }
    }
}


