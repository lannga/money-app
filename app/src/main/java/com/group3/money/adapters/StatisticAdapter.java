package com.group3.money.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group3.money.R;
import com.group3.money.entity.VStatistic;

import java.util.List;

public class StatisticAdapter extends RecyclerView.Adapter<StatisticAdapter.ViewHolder> {

    private List<VStatistic> vStatistics;

    public StatisticAdapter(List<VStatistic> vStatistics) {
        this.vStatistics = vStatistics;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_statistic, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VStatistic vStatistic = vStatistics.get(position);
        holder.categoryIcon.setImageResource(vStatistic.getCategoryIcon());
        holder.amount.setText(String.valueOf(vStatistic.getTotal()) + " đ");
        holder.categoryName.setText(vStatistic.getCategoryName());
        if (vStatistic.getCategoryRootId() != null) {
            holder.childSignLnl.setVisibility(View.VISIBLE);
            if (position + 1 == vStatistics.size()) {
                holder.underCS.setVisibility(View.INVISIBLE);
                return;
            } else {
                holder.underCS.setVisibility(View.VISIBLE);
            }
            VStatistic nextCategory = vStatistics.get(position + 1);
            if (nextCategory.getCategoryRootId() == null) {
                holder.underCS.setVisibility(View.INVISIBLE);
            } else {
                holder.underCS.setVisibility(View.VISIBLE);
            }
        } else {
            holder.childSignLnl.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return vStatistics.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView categoryIcon;
        public TextView categoryName;
        public TextView amount;

        public LinearLayout childSignLnl;

        public View aboveCS, underCS;

        public ViewHolder(View itemView){
            super(itemView);
            categoryIcon = itemView.findViewById(R.id.ic_cat);
            categoryName = itemView.findViewById(R.id.cat_name);
            amount = itemView.findViewById(R.id.amount);

            childSignLnl = itemView.findViewById(R.id.child_sign_lnl);
            aboveCS = itemView.findViewById(R.id.above_v);
            underCS = itemView.findViewById(R.id.under_v);
        }
    }
}
