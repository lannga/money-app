package com.group3.money.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group3.money.R;
import com.group3.money.entity.Base1;
import com.group3.money.entity.VCategory;
import com.group3.money.views.IChoseItemView;

import java.util.List;

public class Base1Adapter extends RecyclerView.Adapter<Base1Adapter.ViewHolder> {
    private List<VCategory> list;
    private IChoseItemView iView;

    public Base1Adapter(List<VCategory> list, IChoseItemView iView) {
        this.list = list;
        this.iView = iView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_basic_1, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VCategory category = list.get(position);
        holder.iconImV.setImageResource(category.getIcon());
        holder.nameTxV.setText(category.getName());
        if (category.getRootId() != null) {
            holder.childSignLnl.setVisibility(View.VISIBLE);
            if (position+1 == list.size()){
                holder.underCS.setVisibility(View.INVISIBLE);
                return;
            }
            else{
                holder.underCS.setVisibility(View.VISIBLE);
            }
            VCategory nextCategory = list.get(position + 1);
            if (nextCategory.getRootId() == null) {
                holder.underCS.setVisibility(View.INVISIBLE);
            }else{
                holder.underCS.setVisibility(View.VISIBLE);
            }
        } else {
            holder.childSignLnl.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView iconImV;
        public TextView nameTxV;
        public LinearLayout menuArea;

        public LinearLayout childSignLnl;

        public View aboveCS, underCS;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iconImV = itemView.findViewById(R.id.icon_imv);
            nameTxV = itemView.findViewById(R.id.name_txv);
//            menuArea = itemView.findViewById(R.id.menu_area);
            childSignLnl = itemView.findViewById(R.id.child_sign_lnl);
            aboveCS = itemView.findViewById(R.id.above_v);
            underCS = itemView.findViewById(R.id.under_v);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Base1 base1 = list.get(getLayoutPosition());
            iView.choseItem(base1);
        }
    }
}
