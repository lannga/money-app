package com.group3.money.adapters;



import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.group3.money.fragments.StatisticFragment;
import com.group3.money.utils.Constants;

public class TimePagerAdapter extends FragmentPagerAdapter {
    private AppCompatActivity appCompatActivity;
    public TimePagerAdapter(FragmentManager fm, AppCompatActivity appCompatActivity) {
        super(fm);
        this.appCompatActivity = appCompatActivity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new StatisticFragment(Constants.DAY, appCompatActivity);
            case 1:
                return new StatisticFragment(Constants.MONTH, appCompatActivity);
            case 2:
                return new StatisticFragment(Constants.YEAR, appCompatActivity);
            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return 3;
    }
}
