package com.group3.money.adapters;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.group3.money.views.IDeleteItemView;

public class SwipeController extends ItemTouchHelper.Callback {

    private RecyclerView.Adapter adapter;
    private IDeleteItemView iDeleteItemView;

    public SwipeController(RecyclerView.Adapter adapter, IDeleteItemView iDeleteItemView) {
        this.adapter = adapter;
        this.iDeleteItemView = iDeleteItemView;
    }


    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        return makeMovementFlags(0, ItemTouchHelper.LEFT);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        iDeleteItemView.showNotification(viewHolder.getAdapterPosition());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

}
