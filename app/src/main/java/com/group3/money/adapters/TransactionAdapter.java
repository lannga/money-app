package com.group3.money.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group3.money.R;
import com.group3.money.entity.VTransaction;
import com.group3.money.utils.Constants;
import com.group3.money.utils.Format;
import com.group3.money.views.IListView;

import java.util.List;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {

    private List<VTransaction> vTransactions;
    private IListView iListView;

    public TransactionAdapter(List<VTransaction> vTransactions, IListView iListView) {
        this.vTransactions = vTransactions;
        this.iListView = iListView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transaction, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VTransaction vTransaction = vTransactions.get(position);
        holder.iconImv.setImageResource(vTransaction.getCategoryIcon());
        holder.nameTxV.setText(vTransaction.getCategoryName());
        holder.timeTxv.setText(Constants.type[vTransaction.getType()]+" - "+Format.stringDate(vTransaction.getTime(), Constants.TIME_PATTERN[0]));
        holder.amountTxV.setText(vTransaction.getAmount()+"  đ");
        if(vTransaction.getNote() != "" && vTransaction.getNote() != null ){
            holder.noteTxV.setText(vTransaction.getNote());
        }else{
            holder.noteTxV.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return vTransactions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView iconImv, menuImv;
        public TextView nameTxV, timeTxv,amountTxV, noteTxV, updateTxv, removeTxt;
        public LinearLayout optionMenuLnL;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            menuImv = itemView.findViewById(R.id.menu_im);
            iconImv = itemView.findViewById(R.id.icon_imv);
            nameTxV = itemView.findViewById(R.id.name_tv);
            timeTxv = itemView.findViewById(R.id.time_tv);
            amountTxV = itemView.findViewById(R.id.amount_tv);
            noteTxV = itemView.findViewById(R.id.note_tv);
            updateTxv = itemView.findViewById(R.id.update_tv);
            removeTxt = itemView.findViewById(R.id.remove_tv);
            optionMenuLnL = itemView.findViewById(R.id.menu_option_lnl);

            updateTxv.setOnClickListener(this);
            removeTxt.setOnClickListener(this);

            menuImv.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            VTransaction vTransaction = vTransactions.get(getLayoutPosition());
            switch (view.getId()){
                case R.id.menu_im:
                    if(optionMenuLnL.getVisibility() == View.GONE){
                        optionMenuLnL.setVisibility(View.VISIBLE);
                    }else {
                        optionMenuLnL.setVisibility(View.GONE);
                    }
                    break;
                case R.id.update_tv:
                    optionMenuLnL.setVisibility(View.GONE);
                    iListView.changeItem(vTransaction);
                    break;
                case R.id.remove_tv:
                    optionMenuLnL.setVisibility(View.GONE);
                    iListView.getDeleteItem(vTransaction);
                    break;
            }
        }
    }
}
