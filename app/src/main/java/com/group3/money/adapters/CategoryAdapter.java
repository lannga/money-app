package com.group3.money.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group3.money.R;
import com.group3.money.entity.VCategory;
import com.group3.money.views.IListView;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private List<VCategory> categoryList;
    private IListView iListView;

    public CategoryAdapter(List<VCategory> categoryList, IListView iListView) {
        this.categoryList = categoryList;
        this.iListView = iListView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_basic_1, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VCategory category = categoryList.get(position);

        holder.name.setText(category.getName());
        holder.icon.setImageResource(category.getIcon());
        if (category.getRootId() != null) {
            holder.childSignLnl.setVisibility(View.VISIBLE);
            if (position + 1 == categoryList.size()) {
                holder.underCS.setVisibility(View.INVISIBLE);
                return;
            } else {
                holder.underCS.setVisibility(View.VISIBLE);
            }
            VCategory nextCategory = categoryList.get(position + 1);
            if (nextCategory.getRootId() == null) {
                holder.underCS.setVisibility(View.INVISIBLE);
            } else {
                holder.underCS.setVisibility(View.VISIBLE);
            }
        } else {
            holder.childSignLnl.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView icon, menuIm;
        public TextView name, updateTv, removeTv;

        public LinearLayout menuLnl;

        public LinearLayout childSignLnl;

        public View aboveCS, underCS;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon_imv);
            name = itemView.findViewById(R.id.name_txv);
            menuIm = itemView.findViewById(R.id.menu_im);
            updateTv = itemView.findViewById(R.id.update_tv);
            removeTv = itemView.findViewById(R.id.remove_tv);
            menuLnl = itemView.findViewById(R.id.menu_option_lnl);
            childSignLnl = itemView.findViewById(R.id.child_sign_lnl);
            aboveCS = itemView.findViewById(R.id.above_v);
            underCS = itemView.findViewById(R.id.under_v);
            menuIm.setOnClickListener(this);
            updateTv.setOnClickListener(this);
            removeTv.setOnClickListener(this);
            menuLnl.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            VCategory vCategory = categoryList.get(getLayoutPosition());
            switch (view.getId()) {
                case R.id.menu_im:
                    if (menuLnl.getVisibility() == View.GONE) {
                        menuLnl.setVisibility(View.VISIBLE);
                    } else {
                        menuLnl.setVisibility(View.GONE);
                    }
                    break;
                case R.id.update_tv:
                    menuLnl.setVisibility(View.GONE);
                    iListView.changeItem(vCategory);
                    break;
                case R.id.remove_tv:
                    menuLnl.setVisibility(View.GONE);
                    iListView.getDeleteItem(vCategory);
                    break;
            }
        }
    }
}
