package com.group3.money.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.group3.money.R;
import com.group3.money.entity.Base1;
import com.group3.money.views.IChoseItemView;

import java.util.List;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.ViewHolder> {
    private List<Base1> list;
    private IChoseItemView iView;

    public GroupAdapter(Object list, IChoseItemView iView) {
        this.list =(List<Base1>) list;
        this.iView = iView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Base1 base1 = list.get(position);
        holder.iconImV.setImageResource(base1.getIcon());
        holder.nameTxV.setText(base1.getName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView iconImV;
        public TextView nameTxV;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iconImV = itemView.findViewById(R.id.icon_imv);
            nameTxV = itemView.findViewById(R.id.name_txv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Base1 base1 = list.get(getLayoutPosition());
            iView.choseItem(base1);
        }
    }
}
