package com.group3.money.fragments;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.group3.money.R;
import com.group3.money.adapters.CategoryAdapter;
import com.group3.money.adapters.SwipeController;
import com.group3.money.entity.DGroup;
import com.group3.money.entity.VCategory;
import com.group3.money.presenters.CategoryListPresenter;
import com.group3.money.utils.GroupPreference;
import com.group3.money.views.IListView;

import java.util.LinkedList;
import java.util.List;
public class CatTypeFragment extends Fragment implements IListView {


    private int type;

    private RecyclerView recyclerView;
    private CategoryAdapter categoryAdapter;
    private List<VCategory> vCategoryList;
    private CategoryListPresenter categoryListPresenter;

    private DGroup currentGroup;


    public CatTypeFragment(int type, AppCompatActivity appCompatActivity) {
        this.type = type;
        currentGroup = GroupPreference.getCurentGroup(appCompatActivity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cat_list, container, false);
        categoryListPresenter = new CategoryListPresenter(this, getActivity());
        vCategoryList= new LinkedList<>();
        categoryAdapter = new CategoryAdapter(vCategoryList, null);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setAdapter(categoryAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeController());
//        itemTouchHelper.attachToRecyclerView(recyclerView);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        categoryListPresenter.getByType(currentGroup.getId(), type);

    }

    @Override
    public void showList(Object... list) {
        vCategoryList.clear();
        vCategoryList.addAll((List<VCategory>) list[0]);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                categoryAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void changeItem(Object item) {

    }

    @Override
    public void getDeleteItem(Object item) {

    }

    @Override
    public void delete() {

    }

}
