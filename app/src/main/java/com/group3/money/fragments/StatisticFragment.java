package com.group3.money.fragments;

import android.animation.ValueAnimator;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.ChangeBounds;
import androidx.transition.ChangeImageTransform;
import androidx.transition.ChangeTransform;
import androidx.transition.Fade;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;

import com.group3.money.R;
import com.group3.money.adapters.StatisticAdapter;
import com.group3.money.dialog.DateDialog;
import com.group3.money.entity.DGroup;
import com.group3.money.entity.VStatistic;
import com.group3.money.presenters.StatisticListPresenter;
import com.group3.money.utils.Constants;
import com.group3.money.utils.Format;
import com.group3.money.utils.GroupPreference;
import com.group3.money.views.IChoseDateView;
import com.group3.money.views.IListView;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class StatisticFragment extends Fragment implements IListView, IChoseDateView,
        View.OnClickListener {
    private Calendar calendar = Calendar.getInstance();
    private EditText timeEdT;
    private ImageView increaseImV, decreaseImV;
    private RecyclerView expenseRcV, incomeRcV;
    private StatisticAdapter expenseAdapter, incomeAdapter;

    private TextView expenseTv, incomeTv;

    private LinearLayout lnl;

    private DateDialog dateDialog;
    private StatisticListPresenter statisticListPresenter;

    private List<VStatistic> expenses, incomes;
    private int index;

    private DGroup currentGroup;

    private int[] indexs ={Calendar.DATE, Calendar.MONTH, Calendar.YEAR};

    public StatisticFragment(int index, AppCompatActivity appCompatActivity) {
        this.index = index;
        currentGroup = GroupPreference.getCurentGroup(appCompatActivity);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_statistic, container, false);

        timeEdT = view.findViewById(R.id.time_edt);
        increaseImV = view.findViewById(R.id.increase_imv);
        decreaseImV = view.findViewById(R.id.decrease_imv);
        expenseRcV = view.findViewById(R.id.expense_rcv);
        incomeRcV = view.findViewById(R.id.income_rcv);

        expenseTv = view.findViewById(R.id.expense_tv);
        incomeTv = view.findViewById(R.id.income_tv);

        lnl = view.findViewById(R.id.lnl);

        timeEdT.setText(Format.stringDate(calendar,Constants.TIME_PATTERN[index]));

        dateDialog = new DateDialog(this, calendar);

        expenses = new LinkedList<>();
        incomes = new LinkedList<>();

        expenseAdapter = new StatisticAdapter(expenses);
        incomeAdapter = new StatisticAdapter(incomes);

        expenseRcV.setLayoutManager(new LinearLayoutManager(getActivity()));
        expenseRcV.setAdapter(expenseAdapter);

        incomeRcV.setLayoutManager(new LinearLayoutManager(getActivity()));
        incomeRcV.setAdapter(incomeAdapter);


        statisticListPresenter = new StatisticListPresenter(this, getActivity());

        timeEdT.setOnClickListener(this);
        expenseTv.setOnClickListener(this);
        incomeTv.setOnClickListener(this);
        increaseImV.setOnClickListener(this);
        decreaseImV.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        statisticListPresenter.getList(currentGroup.getId(), calendar, index);
    }

    @Override
    public void showList(Object... list) {
        List<VStatistic> expenses =(List<VStatistic>) list[0];
        List<VStatistic> incomes =(List<VStatistic>) list[1];
        this.expenses.clear();
        this.expenses.addAll(expenses);
        this.incomes.clear();
        this.incomes.addAll(incomes);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                expenseAdapter.notifyDataSetChanged();
                incomeAdapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void changeItem(Object item) {

    }

    @Override
    public void getDeleteItem(Object item) {

    }

    @Override
    public void choseDate(Calendar c) {
        timeEdT.setText(Format.stringDate(calendar,Constants.TIME_PATTERN[index]));
        statisticListPresenter.getList(currentGroup.getId(), calendar, index);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.time_edt:
                dateDialog.show(getFragmentManager(), Constants.TIME_PATTERN[index]);
                break;
            case R.id.increase_imv:
                calendar.add(indexs[index], 1);
                timeEdT.setText(Format.stringDate(calendar,Constants.TIME_PATTERN[index]));
                statisticListPresenter.getList(currentGroup.getId(),calendar, index);
                break;
            case R.id.decrease_imv:
                calendar.add(indexs[index], -1);
                timeEdT.setText(Format.stringDate(calendar,Constants.TIME_PATTERN[index]));
                statisticListPresenter.getList(currentGroup.getId(),calendar, index);
                break;
            case R.id.expense_tv:
                TransitionManager.beginDelayedTransition(lnl);
                incomeRcV.setVisibility(View.GONE);
                expenseRcV.setVisibility(View.VISIBLE);
                lnl.removeView(incomeTv);
                lnl.removeView(incomeRcV);
                lnl.addView(incomeTv);
                lnl.addView(incomeRcV);
                break;
            case R.id.income_tv:
                TransitionManager.beginDelayedTransition(lnl);
                expenseRcV.setVisibility(View.GONE);
                incomeRcV.setVisibility(View.VISIBLE);
                lnl.removeView(expenseTv);
                lnl.removeView(expenseRcV);
                lnl.addView(expenseTv);
                lnl.addView(expenseRcV);
                break;
            default:
                return;
        }
    }

}
