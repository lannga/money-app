package com.group3.money.modals;

import com.group3.money.R;

public class Category {
    private int id;
    private String name;
    private int icon = R.drawable.ic_food;
    private Category root = null;


    public Category(int id, String name, int icon, Category root) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.root = root;
    }

    public Category(int id, String name, int icon) {
        this.id = id;
        this.name = name;
        this.icon = icon;
    }

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public Category getRoot() {
        return root;
    }

    public void setRoot(Category root) {
        this.root = root;
    }
}
