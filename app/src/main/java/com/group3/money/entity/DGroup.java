package com.group3.money.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.UUID;

@Entity
public class DGroup extends Base1 {

    @PrimaryKey
    @NonNull
    private String id;

    public DGroup(String id, String name, int icon) {
        super(name,icon);
        this.id = id;

    }

    @Ignore
    public DGroup(int icon) {
        super(icon);
        this.id = UUID.randomUUID().toString();

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
