package com.group3.money.entity;

import androidx.annotation.NonNull;
import androidx.room.DatabaseView;

@DatabaseView(
        "SELECT c.*, r.name AS categoryName, r.icon AS categoryIcon, r.rootId AS rootCategoryId " +
        "FROM DTransaction AS c LEFT JOIN DCategory AS r " +
        "ON c.categoryId = r.id ")
public class VTransaction extends DTransaction {

    private int categoryIcon;
    private String categoryName;
    private String rootCategoryId;

    public int getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(int categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getRootCategoryId() {
        return rootCategoryId;
    }

    public void setRootCategoryId(String rootCategoryId) {
        this.rootCategoryId = rootCategoryId;
    }


    public VTransaction(@NonNull String id, String groupId, int type, String categoryId, int amount, long time, String note, int categoryIcon, String categoryName, String rootCategoryId) {
        super(id, groupId, type, categoryId, amount, time, note);
        this.categoryIcon = categoryIcon;
        this.categoryName = categoryName;
        this.rootCategoryId = rootCategoryId;
    }

}
