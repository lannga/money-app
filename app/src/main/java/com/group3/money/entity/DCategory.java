package com.group3.money.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.UUID;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys= {
        @ForeignKey(
            entity = DCategory.class,
            parentColumns = "id",
            childColumns = "rootId",
            onDelete = CASCADE
        ),
        @ForeignKey(
                entity = DGroup.class,
                parentColumns = "id",
                childColumns = "groupId",
                onDelete = CASCADE
        )
})
public class DCategory implements Serializable {

    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    private int icon;
    @ColumnInfo(index = true)
    private String rootId;
    private int type;
    @ColumnInfo(index = true)
    private String groupId;


    public DCategory(String id, String name, int icon, String rootId, int type, String groupId) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.rootId = rootId;
        this.type = type;
        this.groupId = groupId;
    }

    @Ignore
    public DCategory(String groupId, int type) {
        this.id = UUID.randomUUID().toString();
        this.type = type;
        this.groupId = groupId;
    }

    @Ignore
    public DCategory(String name, int icon, String rootId, int type, String groupId) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.icon = icon;
        this.rootId = rootId;
        this.type = type;
        this.groupId = groupId;
    }

    @Ignore
    public DCategory(){
        this.id = UUID.randomUUID().toString();
    }


    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getRootId() {
        return rootId;
    }

    public void setRootId(String rootId) {
        this.rootId = rootId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
