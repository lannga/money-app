package com.group3.money.entity;

import androidx.room.DatabaseView;

import java.io.Serializable;

@DatabaseView(
        "SELECT c.*, r.name AS rootName, r.icon AS rootIcon " +
        "FROM DCategory AS c LEFT JOIN DCategory AS r " +
        "ON c.rootId = r.id "
)
public class VCategory extends Base1 implements Serializable {
    private String id;
    private int type;
    private String rootId;
    private String groupId;
    private String rootName;
    private String rootIcon;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getRootId() {
        return rootId;
    }

    public void setRootId(String rootId) {
        this.rootId = rootId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getRootName() {
        return rootName;
    }

    public void setRootName(String rootName) {
        this.rootName = rootName;
    }

    public String getRootIcon() {
        return rootIcon;
    }

    public void setRootIcon(String rootIcon) {
        this.rootIcon = rootIcon;
    }

    public VCategory(String id, String name, int icon, String rootId, int type, String groupId, String rootName, String rootIcon) {
        super(name, icon);
        this.id = id;
        this.rootId = rootId;
        this.type = type;
        this.groupId = groupId;
        this.rootName = rootName;
        this.rootIcon = rootIcon;
    }
}


