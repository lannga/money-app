package com.group3.money.entity;



public class VStatistic {
    private String categoryId;
    private String rootCategoryId;
    private int categoryIcon;
    private String categoryName;
    private int total;
    private int type;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryRootId() {
        return rootCategoryId;
    }

    public void setCategoryRootId(String rootCategoryId) {
        this.rootCategoryId = rootCategoryId;
    }

    public int getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(int categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public VStatistic(String categoryId, String rootCategoryId, int categoryIcon, String categoryName, int total, int type) {
        this.categoryId = categoryId;
        this.rootCategoryId = rootCategoryId;
        this.categoryIcon = categoryIcon;
        this.categoryName = categoryName;
        this.total = total;
        this.type = type;
    }

    public VStatistic(DCategory dCategory){
        this.categoryId = dCategory.getId();
        this.rootCategoryId = dCategory.getRootId();
        this.categoryIcon = dCategory.getIcon();
        this.categoryName = dCategory.getName();
    }
}
