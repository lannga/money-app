package com.group3.money.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.UUID;

@Entity(foreignKeys = {
        @ForeignKey(
                entity = DGroup.class,
                parentColumns = "id",
                childColumns = "groupId"),
        @ForeignKey(
                entity = DCategory.class,
                parentColumns = "id",
                childColumns = "categoryId"
        )
})
public class DTransaction implements Serializable {

    @PrimaryKey
    @NonNull
    private String id;
    @ColumnInfo(index = true)
    private String groupId;
    private int type;
    @ColumnInfo(index = true)
    private String categoryId;
    private int amount;
    private long time;
    private String note;

    public DTransaction(@NonNull String id, String groupId, int type, String categoryId, int amount, long time, String note) {
        this.id = id;
        this.groupId = groupId;
        this.type = type;
        this.categoryId = categoryId;
        this.amount = amount;
        this.time = time;
        this.note = note;
    }

    @Ignore
    public DTransaction(String groupId, int type, long time) {
        this.id = UUID.randomUUID().toString();
        this.type = type;
        this.groupId = groupId;
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
